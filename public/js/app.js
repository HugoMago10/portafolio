const container = document.getElementById('matrix');

function createAndAnimateElements() {
  // Eliminar los elementos existentes
  const chars = container.querySelectorAll('.bit');
  chars.forEach((char) => char.remove());

  const bits = ['0', '1'];

  // Calculamos la cantidad de caracteres que caben en cada fila y la cantidad de filas que necesitamos
  const charWidth = 12;
  const charHeight = 16;
  const cols = Math.floor(container.clientWidth / charWidth);//cantidad de caracteres que se colocaran en col
  const rows = Math.floor(container.clientHeight / charHeight);

  // Creamos un div para cada caracter
  for (let i = 0; i < cols * 2; i++) {
    const char = document.createElement('div');
    char.innerText = bits[Math.floor(Math.random() * bits.length)];
    char.classList.add('bit');
    char.style.left = (i % cols) * charWidth + 'px';
    char.style.top = (i % rows) * charHeight + 'px';
    container.appendChild(char);
  }

  // Añadimos la animación
  const charsList = container.querySelectorAll('.bit');
  charsList.forEach((char) => {
    char.animate(
      [{ transform: 'translateY(-50px) translateX(0px)', opacity: 1 }, { transform: `translateY(${container.clientHeight}px) translateX(0px)`, opacity: 0 }],
      { duration: Math.random() * 8000 + 1000, iterations: Infinity }
    );
  });

}

createAndAnimateElements();
setInterval(createAndAnimateElements, 10000);

/*************** Elemento nav *****************/
const nav = document.querySelector('#matrix nav');

window.addEventListener('scroll', (e) => {
  if (window.scrollY > 0) {
    nav.classList.add('nav-fixed');
  } else {
    nav.classList.remove('nav-fixed');
  }
});


const hamburger = document.querySelector('#matrix nav .buttom-menu');
const mobile_menu = document.querySelector('#matrix nav ul');
const menu_item = document.querySelectorAll('#matrix nav ul li a');

hamburger.addEventListener('click', () => {
  hamburger.classList.toggle('active');
  mobile_menu.classList.toggle('active');
});


menu_item.forEach((item) => {
  item.addEventListener('click', () => {
    hamburger.classList.toggle('active');
    mobile_menu.classList.toggle('active');
  });
});

document.getElementById('descarga-cv').addEventListener('click', function () {
  // Ruta del archivo PDF de tu currículum
  var rutaArchivo = 'resources/CurriculumH.pdf';

  // Crear un enlace temporal
  var link = document.createElement('a');
  link.href = rutaArchivo;
  link.download = 'curriculum.pdf';

  // Simular un evento de clic en el enlace para iniciar la descarga
  var clicEvent = new MouseEvent('click', {
    view: window,
    bubbles: true,
    cancelable: true
  });
  link.dispatchEvent(clicEvent);
});
