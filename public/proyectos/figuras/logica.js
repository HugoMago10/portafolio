//**************************Clase Parent Figura***********************************************************/
class Figura{
    constructor ( x, y, color ){
        this.punto = {
            x, y
        };
        this.color = color;
        this.canva = document.getElementById("myCanvas");
        this.ctx = this.canva.getContext("2d");
        /* this.ctx.clearRect( 0, 0, this.canva.width, this.canva.height ); */
    }

    /*metodos que se comportan de manera diferente*/
    area (){} 
    perimetro(){}
    nombre(){}
    dibuja (){ }
}

//**************************Clase Rectangulo***********************************************************/
 class Rectangulo extends Figura{
    constructor ( x=100, y=70, ancho=160, altura=110, color='#004000' ){
        super ( x, y, color );
        this.ancho = ancho;
        this.altura = altura;
    }

    set medidas ( lado ){
        const lados = lado.split(' ');
        this.ancho = lados[0];
        this.altura = lados[1];
    }

    perimetro (){ return 2*this.ancho + 2*this.altura; }
    area (){ return this.ancho * this.altura; }
    nombre(){ return "Rectangulo"; }
    dibuja (){
        this.ctx.clearRect( 0, 0, this.canva.width, this.canva.height );
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect( this.punto.x, this.punto.y, this.ancho, this.altura );
    }
}

//**************************Clase Cuadrado***********************************************************/
class Cuadrado extends Figura{
    constructor (x=120, y=60, lado=130, color='#004000'){
        super (x, y, color );
        this.lado = lado;
        this.canva = document.getElementById("myCanvas");
        this.ctx = this.canva.getContext("2d");
        /* this.ctx.clearRect( 0, 0, this.canva.width, this.canva.height ); */
    }

    perimetro (){ return 4*this.lado; }
    area(){ return this.lado*this.lado; }
    nombre(){ return "Cuadrado"; }
    dibuja(){
        this.ctx.clearRect( 0, 0, this.canva.width, this.canva.height );
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect( this.punto.x, this.punto.y, this.lado, this.lado );
    }
}

//**************************Clase Circulo***********************************************************/
class Circulo extends Figura{
    constructor ( x=170, y=120, radio=70, color='#004000' ){
        super  (x, y, color );
        this.radio = radio;
    }

    perimetro (){ return 2* Math.PI* this.radio; }
    area (){ return Math.PI*( Math.pow( this.radio, 2) ); }
    nombre (){ return "Circulo"; }
    dibuja(){
        this.ctx.beginPath();
        this.ctx.clearRect( 0, 0, this.canva.width, this.canva.height );
        this.ctx.fillStyle = this.color;
        this.ctx.arc ( this.punto.x, this.punto.y, this.radio, 0, 2*Math.PI );
        this.ctx.fill();
    }
}

//**************************Clase Triangulo***********************************************************/
class Triangulo extends Figura{
    constructor (x=190, y=40, base=160, altura=140, color='#004000'){
        super ( x, y, color );
        this.base = base;
        this.altura = altura;
    }

    perimetro (){ return 3*this.base; }
    area (){ return (this.base*this.altura)/2; }
    nombre (){ return "Triangulo"; }
    dibuja(){
        this.ctx.fillStyle = this.color;
        this.ctx.beginPath();
        this.ctx.clearRect( 0, 0, this.canva.width, this.canva.height );
        this.ctx.moveTo (parseInt(this.punto.x), parseInt(this.punto.y) );
        this.ctx.lineTo (parseInt(this.punto.x) - (parseInt(this.base/2)), parseInt(this.punto.y)+ parseInt(this.altura) );
        this.ctx.lineTo (parseInt(this.punto.x) + (parseInt(this.base/2)), parseInt(this.punto.y) + parseInt(this.altura) );
        this.ctx.closePath();
        this.ctx.fill();
    }
}

//**************************Clase Rombo***********************************************************/
class Rombo extends Figura{
    constructor ( x=190, y=40, diaMayor=160, diaMenor=70, color='#004000'){
        super ( x, y, color );
        this.diaMayor = diaMayor;
        this.diaMenor = diaMenor;
        this.lado = Math.sqrt ( Math.pow(this.diaMenor, 2 ) + Math.pow(this.diaMayor, 2 ) );
    }

    perimetro(){
        this.lado = Math.sqrt ( Math.pow(this.diaMenor, 2 ) + Math.pow(this.diaMayor, 2 ) );
        return this.lado * 4;
    }
    area(){ return ( this.diaMayor * this.diaMenor )/2; }
    nombre (){ return "Rombo"; }
    dibuja (){   
        this.ctx.fillStyle = this.color;
        this.ctx.beginPath();
        this.ctx.clearRect( 0, 0, this.canva.width, this.canva.height );
        this.ctx.moveTo( parseInt(this.punto.x), parseInt(this.punto.y) );
        this.ctx.lineTo ( parseInt(this.punto.x) + parseInt(this.diaMenor), parseInt(this.punto.y)+ parseInt(this.diaMayor)/2 );
        this.ctx.lineTo ( parseInt(this.punto.x), parseInt(this.punto.y) + parseInt(this.diaMayor) );
        this.ctx.lineTo ( parseInt(this.punto.x) - this.diaMenor, parseInt(this.punto.y) + parseInt(this.diaMayor)/2 );
        this.ctx.closePath();
        this.ctx.fill(); 
    }
}
//********************* Clase Poligono Regular******************************************** */
class PoligonoRegular extends Figura{
    constructor ( x=170, y=120, numLados=5, lado=90, color='#004000' ){
        super ( x, y, color );
        this.numLados = numLados;
        this.lado = lado;
        this.angulo = ( ( 360 / this.numLados )/2 ) * (Math.PI/180);
        this.apotema = this.lado / ( 2 * Math.tan ( this.angulo  )  );
        this.nombres = ["",,"","Triángulo","Cuadrilátero","Pentágono","Hexágono","Héptagono","Octagono","Enéagono",
            "Decágono","Endecágono","Dodecágono","Tridecágono","Tetradecágono","Pentadecágono","Hexadecágono"];
    }

    perimetro(){ return this.lado * this.numLados; }
    area(){
        this.angulo = ( ( 360 / this.numLados )/2 ) * (Math.PI/180);
        this.apotema = this.lado / ( 2 * Math.tan ( this.angulo  )  );
        return ( this.perimetro() * this.apotema ) / 2;
    }

    getAngulo (){
        return this.angulo = ( 360 / this.numLados );
    }

    dibuja(){
        this.ctx.fillStyle = this.color;
        var rad = ( 2*Math.PI / parseInt(this.numLados) );//Angulo de 60 grados

        this.ctx.beginPath();
            this.ctx.clearRect( 0, 0, this.canva.width, this.canva.height );
		    for( var i = 0; i < parseInt( this.numLados); i++ ){
                var x = parseInt( this.punto.x) + parseInt( this.lado ) * Math.cos( rad*i );
                var y = parseInt( this.punto.y) + parseInt( this.lado ) * Math.sin( rad*i );
                this.ctx.lineTo( x, y );
            }
        this.ctx.closePath();
        this.ctx.fill();
    }
    
}

//********************Definicion de Figuras************************************************************* */
var tipoFig = {
    'figura':`<p></strong>La geometría</strong> Se deriva de los vocablos griegos geos (tierra) y metron (medida). Los
        antiguos egipcios, chinos, babilonios, romanos y griegos utilizaron la geometria en la agrimesurada, navegación,
        atronomía y otras labores prácticas.</p><p>Un plano tiene longitud y anchura pero no espesor. Un plano es una
        superficie plana.</p>`,
    'cuadrado' : `<strong>Cuadrado</strong><p>Se llama cuadrado al paralelogramo que tiene sus cuatros ángulos y sus
        cuatro lados congruente. Sus diagonales se cortan en partes congruentes. La suma de sus ángulos interiores es
        de 360°</p>`,
    'triangulo': `<strong>Triangulo</strong><p>Es un poligono de 3 lados, que viene determinado por tres puntos no
        colineales llamados vertices. La suma de los tres ángulos de un triangulo es de 180°</p>`,
    'circulo':`<strong>Circunferencia</strong><p>La circunferencia es el conjunto de todos los puntos del plano cuya la
        distancia de otro plano (centro), es constante(radio)</p>`,
    'rombo':`<strong>Rombo</strong><p>Se llama rombo al parelogramo que tiene sus cuatro lados congruentes. La condición
        necesaria y suficiente para que un palelogramo sea rombo es que tenga dos lados consecutivos congruentes.</p>
        <p>Sus diagonales son perpendiculares y bisectrices de los ángulos cuyos vértices unen.</p>`,
    'rectangulo':`<strong>Rectangulo</strong><p>Se llama rectángulo al paralelogramo que tiene sus cuatro ángulos rectos
        </p>`,
    'poligono':`<p>Un Poligono Regular es el que tiene los lados y los ángulos interiores iguales</p>
        <p>La apotema del polígono regular es el segmento que une el centro del poligono con el punto medio de un
        lado.</p><p>El ángulo central de un poligono regular de n lados mide: 360°/n.</p>`
}

/*********************** Declaracion de variables *************************************/
var colorFig = "#004000";
var select = document.getElementById("selectFigura");
var figuras = [];
figuras[0] = new Cuadrado();
figuras[1] = new Circulo ();
figuras[2] = new Rombo ( );
figuras[3] = new Rectangulo( );
figuras[4] = new Triangulo( );
figuras[5] = new PoligonoRegular( );
document.getElementById ('respuesta').innerHTML = tipoFig.figura;

//Parte en donde se desplegara la respuesta
const respuesta = document.getElementById ('respuesta');

/************************ Evento del select ********************************************/
select.addEventListener('change', function(){
    seleccionFigura ( );

    const submit = document.getElementById('formulario').addEventListener('submit',function(e){
        e.preventDefault();
        switch ( select.value ){
            case 'cuadrado':
                document.getElementById ('respuesta').innerHTML = tipoFig.cuadrado + "<p>Area: "+figuras[0].area().toFixed(2) +
                "</p><p>Perimetro: "+ figuras[0].perimetro ().toFixed(2)+"</p>";
                break;
            case 'circulo':
                document.getElementById ('respuesta').innerHTML = tipoFig.circulo + "<p>Area: "+figuras[1].area().toFixed(2) +
                "</p><p>Perimetro: "+ figuras[1].perimetro ().toFixed(2)+"</p>";
                break;
            case 'rombo':
                document.getElementById ('respuesta').innerHTML = tipoFig.rombo + "<p>Area: "+figuras[2].area().toFixed(2) +
                "</p><p>Perimetro: "+ figuras[2].perimetro ().toFixed(2)+"</p>";
                document.getElementsByName ("txtLado")[0].value = figuras[2].lado.toFixed(2);
                break;
            case 'rectangulo':
                document.getElementById ('respuesta').innerHTML = tipoFig.rectangulo + "<p>Area: "+figuras[3].area().toFixed(2) +
                "</p><p>Perimetro: "+ figuras[3].perimetro ().toFixed(2)+"</p>";
                break;
            case 'triangulo':
                document.getElementById ('respuesta').innerHTML = tipoFig.triangulo + "<p>Area: "+figuras[4].area().toFixed(2) +
                "</p><p>Perimetro: "+ figuras[4].perimetro ().toFixed(2)+"</p>";
                break;
            case 'poligono':
                document.getElementById ('respuesta').innerHTML = tipoFig.poligono + "<p>Area: "+figuras[5].area().toFixed(2) +
                "</p><p>Perimetro: "+ figuras[5].perimetro ().toFixed(2)+"</p>";
                document.getElementById ('txtApotema').value = figuras[5].apotema.toFixed(2);
                document.getElementById ('txtAngulo').value = figuras[5].getAngulo().toFixed(2);
                break;
        }
    },false);
})

function seleccionFigura ( ){
    switch ( select.value ){
        case 'cuadrado':
            creaComponente( ['Lado:'], ['txtLado'] );
            setInputX_Y ( figuras[0] );
            document.getElementById ('respuesta').innerHTML = tipoFig.cuadrado;
            document.getElementsByName("txtLado")[0].value = figuras[0].lado;
            figuras[0].dibuja();

            document.getElementById('txtLado').addEventListener('keyup',function(){
                figuras[0].lado = document.getElementById ("txtLado").value;
                figuras[0].dibuja();
            },false);

            document.getElementById('txtColor').addEventListener('change',function(){
                figuras[0].color = document.getElementById('txtColor').value;
                figuras[0].dibuja();
            });

            muevePosicion ( figuras[0] );
            break;
        case 'circulo':
            creaComponente( ['Radio:'], ['txtRadio'] );
            setInputX_Y ( figuras[1] );
            document.getElementById ('respuesta').innerHTML = tipoFig.circulo;
            document.getElementsByName ("txtRadio")[0].value = figuras[1].radio;
            figuras[1].dibuja();
            
            document.getElementById ("txtRadio").addEventListener('keyup', function(){
                figuras[1].radio = document.getElementById("txtRadio").value
                figuras[1].dibuja();
            },false );

            document.getElementById('txtColor').addEventListener('change',function(){
                figuras[1].color = document.getElementById('txtColor').value;
                figuras[1].dibuja();
            });

            muevePosicion ( figuras[1] );
            break;
        case 'rombo':
            creaComponente( ['Diagonal Mayor: ', 'Diagonal Menor: ','Lado: '],
                ['txtDia_mayor', 'txtDia_menor', 'txtLado'] );
            setInputX_Y ( figuras[2] );
            document.getElementById('respuesta').innerHTML = tipoFig.rombo;
            document.getElementsByName ("txtDia_mayor")[0].value = figuras[2].diaMayor;
            document.getElementsByName ("txtDia_menor")[0].value = figuras[2].diaMenor;
            document.getElementById ('txtLado').setAttribute('readonly',"readonly");
            document.getElementsByName ("txtLado")[0].value = figuras[2].lado.toFixed(2);
            figuras[2].dibuja();

            document.getElementById("txtDia_menor").addEventListener('keyup', function (){
                figuras[2].diaMenor = document.getElementById("txtDia_menor").value;
                figuras[2].dibuja();
            });

            document.getElementById("txtDia_mayor").addEventListener('keyup', function (){
                figuras[2].diaMayor = document.getElementById("txtDia_mayor").value;
                figuras[2].dibuja();
            });

            document.getElementById('txtColor').addEventListener('change',function(){
                figuras[2].color = document.getElementById('txtColor').value;
                figuras[2].dibuja();
            });

            muevePosicion ( figuras[2] );
            break;
        case 'rectangulo':
            creaComponente( ['Base: ', 'Altura: '], ['txtAncho', 'txtAltura'] );
            setInputX_Y ( figuras[3] );
            document.getElementById('respuesta').innerHTML = tipoFig.rectangulo;
            document.getElementsByName("txtAncho")[0].value = figuras[3].ancho;
            document.getElementsByName("txtAltura")[0].value = figuras[3].altura;
            figuras[3].dibuja();

            document.getElementById('txtAncho').addEventListener('keyup',function(){
                figuras[3].ancho = document.getElementById('txtAncho').value;
                figuras[3].dibuja();
            });
            document.getElementById('txtAltura').addEventListener('keyup',function(){
                figuras[3].altura = document.getElementById('txtAltura').value;
                figuras[3].dibuja();
            });

            document.getElementById('txtColor').addEventListener('change',function(){
                figuras[3].color = document.getElementById('txtColor').value;
                figuras[3].dibuja();
            });

            muevePosicion (figuras[3]);
            break;
        case 'triangulo':
            creaComponente( ['Base: ', 'Altura: '], ['txtBase', 'txtAltura'] );
            setInputX_Y (figuras[4]);

            document.getElementById('respuesta').innerHTML = tipoFig.triangulo;
            document.getElementsByName("txtBase")[0].value = figuras[4].base;
            document.getElementsByName("txtAltura")[0].value = figuras[4].altura;
            figuras[4].dibuja();

            document.getElementById ('txtBase').addEventListener('keyup',function(){
                figuras[4].base = document.getElementById('txtBase').value;
                figuras[4].dibuja();
            });

            document.getElementById ('txtAltura').addEventListener('keyup',function(){
                figuras[4].altura = document.getElementById('txtAltura').value;
                figuras[4].dibuja();
            });

            document.getElementById('txtColor').addEventListener('change',function(){
                figuras[4].color = document.getElementById('txtColor').value;
                figuras[4].dibuja();
            });

            muevePosicion( figuras[4] );
            break;
        case 'poligono':
            creaComponente( ['Numero de lados:','lado','Apotema: ', 'Angulo: '], ['txtNumLados','txtLado','txtApotema',
                'txtAngulo'] );
            setInputX_Y( figuras[5] );

            document.getElementsByName("txtNumLados")[0].value = figuras[5].numLados;
            document.getElementsByName("txtLado")[0].value = figuras[5].lado;
            document.getElementById ('txtApotema').value = figuras[5].apotema.toFixed(2);
            document.getElementById ('txtAngulo').value = figuras[5].angulo.toFixed(2);
            document.getElementById ('txtApotema').setAttribute('readonly',"readonly");
            document.getElementById ('txtAngulo').setAttribute('readonly',"readonly");
            document.getElementById('respuesta').innerHTML = "<strong>"+figuras[5].nombres[ figuras[5].numLados ]
            +"</strong>"+tipoFig.poligono;
            figuras[5].dibuja();

            document.getElementById('txtNumLados').addEventListener('keyup',function(){
                figuras[5].numLados = document.getElementById('txtNumLados').value;
                var dato = figuras[5].nombres[figuras[5].numLados ];

                document.getElementById('respuesta').innerHTML = dato===undefined ? ('<strong>Poligono</strong>')
                : ("<strong>"+figuras[5].nombres[ figuras[5].numLados ] + "</strong>"+tipoFig.poligono);
                figuras[5].dibuja();
            });
            document.getElementById('txtLado').addEventListener('keyup',function(){
                figuras[5].lado = document.getElementById('txtLado').value;
                figuras[5].dibuja();
            });
            
            document.getElementById('txtColor').addEventListener('change',function(){
                figuras[5].color = document.getElementById('txtColor').value;
                figuras[5].dibuja();
            });

            muevePosicion ( figuras[5] );
            break;
        default:
            console.log ("Error");
        
    }
}

/************************Cambia color de figuras*********************************************************/


//*****************Asignado valores a los input X y Y ****************************************************/
function setInputX_Y ( figura ){
    document.getElementById ("txtX").value = parseInt(figura.punto.x);
    document.getElementById ("txtY").value = parseInt (figura.punto.y);
}
//*********************Moviendo sobre los ejes al objeto***************************************************/

function muevePosicion( figura ){
    document.getElementById("txtX").addEventListener('change',function(){
        figura.punto.x = document.getElementById ("txtX").value;
        figura.dibuja();
    });
    document.getElementById('txtY').addEventListener('change',function(){
        figura.punto.y = document.getElementById ("txtY").value;
        figura.dibuja();
    })
}

/*********************************Crea Componentes de formulario *************************************/

function creaComponente (valores, names){
    //Obtiene el valor de txtNumero, cantidad de inputs a crear
    resultado = document.getElementById("resultado");
    resultado.innerHTML = "";

    //const fragment = document.createDocumentFragment();
    
    const form = document.createElement("form");
    form.setAttribute('id','formulario');
    form.method = "POST";
    form.action = "";
    for (var i = 0; i<valores.length; i++){
        const div = document.createElement ('div');
        div.setAttribute ("class","row form-group px-3");
        const label = document.createElement ('label');
        label.textContent = valores[ i ];
        label.setAttribute ("class","col-sm-5 col-form-label");
        
        const input = document.createElement('input');
        input.setAttribute('type','text');
        input.setAttribute("class","form-control col-sm-7 ");
        input.setAttribute('placeholder',valores[i]);
        input.setAttribute("name",names[i] );
        input.setAttribute("id",names[i] );

        div.appendChild ( label );
        div.appendChild ( input );
        form.appendChild (div);
        
    }

    const div = document.createElement("div");
    div.setAttribute ("class","form-group row py-3");

    const labelColor = document.createElement("label");
    labelColor.textContent = "Color: ";
    const color =document.createElement('input');
    color.setAttribute('id','txtColor');
    color.setAttribute ('type','color');
    color.setAttribute ('value','#004000');
    labelColor.setAttribute ('class','col-sm-2');
    labelColor.appendChild ( color );
    div.appendChild ( labelColor );

    const labelX = document.createElement ("label");
    labelX.textContent = "Posicion X: ";
    labelX.setAttribute ("class","col-sm-5");
    const inputX = document.createElement('input');
    inputX.setAttribute ('id',"txtX");
    inputX.setAttribute ('class',"form-control");
    inputX.setAttribute ('type','number');
    inputX.setAttribute ('min','0');
    inputX.setAttribute ('max','350');
    inputX.setAttribute ('step',"10");
    labelX.appendChild (inputX);
    div.appendChild( labelX );

    const labelY = document.createElement('label');
    labelY.textContent = "Posicion Y: ";
    labelY.setAttribute ("class","col-sm-5");
    const inputY = document.createElement('input');
    inputY.setAttribute('class',"form-control");
    inputY.setAttribute('id',"txtY");
    inputY.setAttribute ('type','number');
    inputY.setAttribute ('min','0');
    inputY.setAttribute ('max','350');
    inputY.setAttribute ('step',"10");
    labelY.appendChild ( inputY );
    div.appendChild( labelY );

    const button = document.createElement('input');
    button.setAttribute ('type','submit');
    button.setAttribute("class","form-control bg bg-primary text-white");
    button.setAttribute ('value', "Calcular");

    form.appendChild (div);
    form.appendChild (button);

    resultado.appendChild (form);
}